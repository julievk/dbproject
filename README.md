# TDT4145 Project
### This repository contains the Java implementation of the TDT4145 Database project.



## Compiling the application
This project uses Maven, and runs on Java 8. To compile use
```
$mvn clean install
```
## Running the application
After compiling the application, it can be executed using the main method in the StartProgram class.

## Database 
The database is located at NTNU servers and the MySQL-database will only accept connections from an NTNU-network or through VPN. <br/>
It can be accesed via mysqladmin.stud.ntnu.no using username: karenhom_piazza, password: Buran123.

## Test user
Email: buran@mail.no <br/>
Password: Buran123 

## Comments
All use cases are covered in the program, but we have some extra classes and methods that a full Piazza program could use. The methods not used in the StartProgram may not be working 100% as desired.<br/>
We are also aware that the methods for getting IDs from the database are not the best. These IDs are auto incrementet when inserting a new row into the given tables, and should be extracted when this is being done using an Statement.RETURN_GENERATED_KEYS. Unfortunatelly we did not have time to fix this issue. 
