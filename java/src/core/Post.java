package core;
import sql.Queries;

import java.sql.Connection;
import java.util.*;

/**
 * Class for Posts.
 */
public class Post{
    private String tag;
    private String colorCode;
    private String content;
    private String folderName;
    private piazzaUser user; 
    private Course course;

    /**
     * Constructor for post. When a Post is made, content is also written to database.
     * The color code is automatically set to blue.
     * @param user
     * @param course
     */
    public Post(piazzaUser user, Course course){
        this.user = user;
        this.course = course;
        this.setColorCode("blue"); //default

        //Write content:
        this.writePost();
    }

    /**
     * Setter for ColorCode.
     * @param color the color for colorcode.
     */
    public void setColorCode(String color){
        this.colorCode = color;
    }

    /**
     * Getter for colorcode.
     * @return colorcode.
     */
    public String getColorCode(){
        return this.colorCode;
    }

    /**
     * Setter for tag.
     */
    public void setTag(){
        try{
            Scanner sc=new Scanner(System.in);
            System.out.println("Choose a tag for your post: ");
            String tag = sc.nextLine();
            this.tag = tag;
        }
        catch (Exception e){System.out.println("Something went wrong. ");}

    }

    /**
     * Getting the tag for this Post instance
     * @return tag
     */
    public String getTag(){
        return this.tag;
    }

    /**
     * Writes post, saves input from user to db and prints msg to user.
     */
    public void writePost(){
        try {
            Scanner sc=new Scanner(System.in);
            System.out.print("Enter content of post: ");
            String str= sc.nextLine();
            this.content = str;
            System.out.println("Post written. ");
        }
        catch (Exception e){
            System.out.println("Unable to write post. ");
        }
    }
    /**
     * Returns the post.
     * @return post´s content.
     */
    public String readPost(){
        return this.content;
    }
    /**
     * Saves post to the database in table Post.
     * ! Step 4 to Use case 2 (last step).
     */
    public void savePost(String tag, String colorCode, String content, String folderID, int courseID, int userID){
        Queries q = new Queries();
        q.savePost(tag, colorCode, content, folderID, courseID, userID);
    }

    /**
     * Selects a folder for post. Gets input from user.
     */
    public void chooseFolder(){
        System.out.println("Please choose a folder for your post: ");
        Queries q = new Queries();
        ArrayList<String> folderName = q.getAllFolders();
        System.out.println(folderName);
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        while (!folderName.contains(name)) {
            System.out.println("Not a valid folder. Try again");
            System.out.println("Please choose a folder for your post: ");
            System.out.println(folderName);
            name = sc.nextLine();
        }
        this.folderName = name;
        System.out.println("Folder "+this.folderName+" chosen.");
    }

    /**
     * Getting the folderName for this Post instance
     * @return folderName
     */
    public String getFolderName(){
        return this.folderName;
    }

    /**
     * Gets courseID from the folderName
     * @param folderName foldername
     * @return courseID as int
     */
    public int getCourseID(String folderName){
        Queries q = new Queries();
        return q.getCourseIDFromFolder(folderName);
    }

    /**
     * Method for searching through posts.
     * Gets search word as input from user via console.
     * Prints the result to console.
     */
    public void searchForPost(){
        System.out.println("Search for posts. Type in search word:");
        Scanner sc = new Scanner(System.in);
        String searchWord = sc.nextLine();
        Queries q = new Queries();
        ArrayList<Integer> postIDs = q.searchPosts(searchWord);
        if(postIDs.isEmpty()) {
            System.out.println("No posts matching.");
        }
        else {
            System.out.println("Posts containing "+searchWord+":\nPostID: Content:");
            for (int postID : postIDs){
                System.out.println("     "+postID+": "+q.getPostContent(postID));
            }
        }
    }

    /**
     * Getter for PostID.
     * @param content content of post.
     * @return
     */
    public int getPostID(String content){
        Queries q = new Queries();
        return q.getPostID(content);
    }

    /**
     * Getter for course.
     * @return Course object.
     */
    public Course getCourse() {
        return course;
    }
}