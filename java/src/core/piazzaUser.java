package core;

import sql.Queries;

import java.sql.SQLOutput;
import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.regex.*;

/**
 * Class for all Users.
 */
public class piazzaUser {

    //private static int userID;
    private String email;
    private String password;
    private ArrayList<UserInCourse> courses = new ArrayList<>();
    private boolean isInstructor;

    /**
     * Constructor for a piazzaUser.
     * @param email email adress.
     * @param password password.
     */
    public piazzaUser(String email, String password, String courseName, boolean isInstructor){
        if (validEmail(email)){
            this.email=email;
            this.password=password;
            this.setIsInstructor(false);
            addUserInCourse(courseName, isInstructor);
        }
        else{
            throw new IllegalArgumentException("Invalid email");
        }

    }

    /**
     * Helper method to check if the email provided is valid
     * @param email email adress.
     * @return boolean, whether the email is valid or not.
     */
    private boolean validEmail(String email){
        return(Pattern.matches("^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$", email));
    }

    /**
     * Uses method in Queries to save the user to the database
     */
    public void saveUser(){
        Queries q = new Queries();
        q.saveUser(this.email, this.password);
    }

    /**
     * Getter for email.
     * @return email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Getter for password.
     * @return password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Getter for isInstructor.
     * @return isInstructor.
     */
    public boolean getisInstructor() {
        return this.isInstructor;
    }

    /**
     * Setter for isInstructor.
     * @param x boolean isInstructor.
     */
    public void setIsInstructor(boolean x) {
        this.isInstructor = x;
    }

    /**
     * Getter for UserID.
     * @param email email.
     * @return int userid.
     */
    public int getUserID(String email){
        Queries q = new Queries();
        return q.getUserID(email);
    }

    /**
     * A user can join a course as an instructor or not.
     * @param CourseName courseName.
     * @param role role in course (isInstructor).
     */
    public void addUserInCourse(String CourseName, boolean role){
        UserInCourse newCourse = new UserInCourse(this.email, CourseName, role);
    }

    /**
     * Getter for Course list.
     * @return Array List of courses.
     */
    public ArrayList<UserInCourse> getCourses(){
        return courses;
    }

}
