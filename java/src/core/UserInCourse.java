package core;

import sql.Queries;

/**
 * Class for table UserInCourse.
 */
public class UserInCourse {

    private int courseID;
    private int userID;
    private boolean isInstructor;

    /**
     * Constructor for USerInCourse.
     * @param email email address.
     * @param courseName course name.
     * @param isInstructor boolean for isInstructor.
     */
    public UserInCourse(String email, String courseName, boolean isInstructor){
        Queries q = new Queries();
        this.courseID = q.getCourseID(courseName);
        this.userID = q.getUserID(email);
        this.isInstructor = isInstructor;
    }

    /**
     * Saves userInCourse in database.
     */
    public void saveUserInCourse(){
        Queries q = new Queries();
        q.saveUserInCourse(userID, courseID, isInstructor);
    }

    /**
     * Getter for isInstructor.
     * @return boolean isInstructor.
     */
    public boolean isInstructor() {
        return isInstructor;
    }

}
