package core;

import sql.Queries;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Scanner;

/**
 * Piazza class. Represents the overall system.
 */
public class Piazza {

    public Piazza() {
    }

    /**
     * Method that lets you create a new user and save it to the database
     * Gets parameters from input
     */
    public void signIn(){
        try {
            System.out.println("Welcome to Piazza! Please sign in");
            System.out.println("Email:");
            System.out.println("Password:");
            System.out.println("Name of the course you want to join:");
            System.out.println("Are you an instructor? Type Y/N");
            Scanner sc = new Scanner(System.in);
            String email = sc.nextLine();
            String password = sc.nextLine();
            String courseName = sc.nextLine();
            String isInstructor = sc.nextLine();
            boolean role = false;
            if (isInstructor.equals("Y")){
                role = true;
            }
            piazzaUser newUser = new piazzaUser(email, password, courseName, role);
            newUser.saveUser();
            System.out.println("Congratulations on your new User");
            System.out.println("You are now signed in");
        }
        catch(IllegalArgumentException e){
            System.out.println(e);
        }
    }

    /**
     * Method for logging in to existing account.
     * Gets parameters from input.
     */
    public void logIn(){
        try {
            Scanner sc = new Scanner(System.in);
            Queries q = new Queries();
            System.out.println("Welcome to Piazza");
            System.out.println("Which course do you wish to log in to? Options:");
            System.out.println(q.getAllCourses());
            String courseName = sc.nextLine();
            while (!checkCourse(courseName)) {
                System.out.println("Not valid course name, try again");
                System.out.println("Which course do you wish to log in to? Options:");
                System.out.println(q.getAllCourses());
                courseName = sc.nextLine();
            }

            System.out.println("Email:");
            String email = sc.nextLine();
            UserInCourse user = new UserInCourse(email,courseName,q.checkIsInstructor(q.getUserID(email),q.getCourseID(courseName)));
            System.out.println("Password:");
            String password = sc.nextLine();

            String role ="";
            if (user.isInstructor()){
                role = "Instructor";
            }
            else{
                role = "Student";
            }

            if (checkLogin(email, password)) {
                System.out.println("Log in to " + courseName + " as " + role + " successful");
            } else {
                System.out.println("Could not log in. Try another password or Sign In if you do not already have a user");
            }
        }
        catch(IllegalArgumentException e){
            System.out.println(e);
        }
    }

    /**
     * Checks if the User trying to log in exists in the database, and that the password is corresponds
     * with the email
     * @param email email adress
     * @param password password
     * @return boolean if login is successful.
     */
    public boolean checkLogin(String email, String password){
        Queries q = new Queries();
        return (q.existingEmail(email) && q.correctPassword(email,password));
    }

    /**
     * Method for printing user statistics to console.
     * Uses query getStats() from Queries.java to collect the data from the database.
     */
    public void getStats(){
        System.out.println("User statistics ordered by read posts:");
        Queries q = new Queries();
        LinkedHashMap<String, ArrayList<Integer>> result = q.getStats();
        for (String email : result.keySet()) {
            System.out.println("User email: "+email+" - Read: "+result.get(email).get(0)+" - Created: "+result.get(email).get(1));
        }
    }

    /**
     * Checks if coursename belongs to valid courseID.
     * @param courseName coursename
     * @return boolean for if courseID is valid.
     */
    public boolean checkCourse(String courseName){
        Queries q = new Queries();
        return q.validCourseID(courseName);
    }
}
