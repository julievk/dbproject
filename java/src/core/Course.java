package core;

import sql.Queries;

import java.util.Collection;

/**
 * Course class. Has name, term, a collection of folders.
 */
public class Course {
    private String name;
    private String term;

    private Collection<Folder> foldersInCourse;

    /**
     * Constructor for Course.
     * @param name name of course.
     * @param term term.
     */
    public Course(String name, String term){
        setName(name);
        setTerm(term);
    }

    /**
     * Setter for name.
     * @param name name
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * Getter for name.
     * @return name
     */
    public String getName(){
        return this.name;
    }

    /**
     * Setter for term.
     * @param term term
     */
    public void setTerm(String term){
        this.term = term;
    }

    /**
     * Getter for term.
     * @return term
     */
    public String getTerm(){
        return this.term;
    }

    /**
     * Getter for CourseID.
     * @return courseID.
     */
    public int getCourseID(){
        Queries q = new Queries();
        return q.getCourseID(this.getName());
    }

    /**
     * Adds a folder to this course. 
     * @param folder the folder getting added to the course.
     */
    public void addFolder(Folder folder){
        this.foldersInCourse.add(folder);
    }
}
