package core;

import sql.Queries;

/**
 * Folder class. Has a name, course and boolean for anonymous-setting.
 */
public class Folder {

    private String name;
    private boolean anonymous;

    private Course course; //each folder belongs to a specific course.

    /**
     * Constructor for Folder. Adds folder to course.
     * @param name name of folder.
     * @param courseID courseID.
     */
    public Folder(String name, int courseID){
        this.setAnonymous(false); //default
        this.setName(name);

        this.course.addFolder(this);
    }

    /**
     * Setter for name.
     * @param name name
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * Getter for name.
     * @return name.
     */
    public String get_Name(){
        return this.name;
    }

    /**
     * Setter for anonymous.
     * @param anon anonymous boolean.
     */
    public void setAnonymous(boolean anon){
        this.anonymous = anon;
    }

    /**
     * Getter for anonymous.
     * @return boolean anonymous.
     */
    public boolean getAnonymous(){
        return this.anonymous;
    }

    /*
     * Setter for courseID.
     * @param courseID couseID
     */
    /*public void setCourse(int courseID){
        Queries q = new Queries();
        this.course = q.getCourse(courseID);
    }*/

    /**
     * Getter for Course object belonging to Folder.
     * @return course.
     */
    public Course getCourse(){
        return this.course;
    }

    /**
     * Getter for FolderID.
     * @return folderID.
     */
    public String getFolderID(){
        return ""+this.name + String.valueOf(this.course.getCourseID());
    }

}