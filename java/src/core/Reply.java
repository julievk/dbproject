package core;

import sql.Queries;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Class for Replies.
 */
public class Reply{
    private Post postRepliedTo;
    private String content;
    private String replyType;
    private piazzaUser user;
    private int courseID;

    /**
     * Constructor. Similar to Post, it creates content of reply and saves it in the object.
     * @param user User writing the post.
     * @param post The post it is a reply to.
     * @param replyType Can be reply, comment, or follow-up discussions.
     */
    public Reply(piazzaUser user, Post post, String replyType){
        this.user = user;
        this.postRepliedTo = post;
        this.setReplyType();
        int userID = this.user.getUserID(user.getEmail());
        String folderName = this.postRepliedTo.getFolderName();
        this.courseID = this.postRepliedTo.getCourseID(folderName);
        //write reply:
        this.writeReply();
    }

    /**
     * Writes a reply from user via scanner. Prints statements to user. Saves to object.
     */
    public void writeReply(){
        try {
            Scanner sc=new Scanner(System.in);
            System.out.print("Enter a reply: ");
            String str= sc.nextLine();
            this.content = str;
            System.out.println("Reply written. ");
        }
        catch (Exception e){
            System.out.println("Unable to write reply. ");
        }
    }

    /**
     * Setter for replyType.
     */
    public void setReplyType(){
        ArrayList<String> types = new ArrayList<>();
        types.add("Reply");
        types.add("Follow-up discussion");
        types.add("Additional comment");
        System.out.println("Choose reply type:\n(Type 1 for "+types.get(0)+", 2 for "+types.get(1)+", 3 for "+types.get(2)+")");
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        String replyType = "Reply"; //default
        if(input.equals("2")) {
            replyType = types.get(1);
        }
        else if (input.equals("3")) {
            replyType = types.get(2);
        }
        this.replyType = replyType;
    }

    /**
     * Getter for replyType.
     * @return replytype.
     */
    public String getReplyType(){
        return this.replyType;
    }

    /**
     * Reads reply.
     * @return content of reply.
     */
    public String readReply(){
        return this.content;
    }

    /**
     * The post´s color code changes to yellow when an instructor replies.
     * Is fired in constructor of Reply.
     */
    private void changePostColor(){
        this.postRepliedTo.setColorCode("yellow");
        Queries q = new Queries();
        q.updateColor(postRepliedTo.getPostID(postRepliedTo.readPost()), postRepliedTo.getColorCode());
    }

    /**
     * Getter for isInstructor. Checks if the user posting the reply is an instructor in the given course.
     * @param userID the userID of the user posting the reply.
     * @param courseID courseID of the subject.
     * @return
     */
    private boolean getIsInstructor(int userID, int courseID){
        Queries q = new Queries();
        return q.checkIsInstructor(userID, courseID);
    }

    /**
     * Saves reply to database.
     * @param postID PostID of the post replied to.
     * @param content Content of reply.
     * @param type ReplyType.
     * @param userID UserID of the user posting the reply.
     */
    public void saveReply(int postID, String content, String type, int userID){
        Queries q = new Queries();
        q.saveReply(postID, content, type, userID);
        //check if user writing reply is an instructor to change the colorCode
        if (getIsInstructor(userID, this.courseID)){
            this.changePostColor();
        }
    }
}

