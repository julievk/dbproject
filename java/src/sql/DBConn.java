package sql;

import java.sql.*;
import java.util.Properties;

public abstract class DBConn {
    protected Connection connection;

    public DBConn() {
        connect();
    }

    public void connect() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Properties p = new Properties();
            p.put("user", "karenhom_piazza");
            p.put("password", "Buran123");
            connection = DriverManager.getConnection("jdbc:mysql://mysql.stud.ntnu.no/karenhom_piazDB?allowPublicKeyRetrieval=true&autoReconnect=true&useSSL=false",p);
        } catch (Exception e)
        {
            throw new RuntimeException("Unable to connect", e);
        }
    }
}


