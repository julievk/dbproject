package sql;

import core.Course;
import core.Folder;
import core.UserInCourse;
import core.piazzaUser;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Queries for program.
 */
public class Queries extends DBConn {

    /**
     * Getter for all CourseIDs.
     * @return Array of all CourseIDs.
     */
    public ArrayList<Integer> getAllCourseID() {

        ArrayList<Integer> existingCourseID = new ArrayList<Integer>();

        try {
            Statement statement = connection.createStatement();

            String sql = "SELECT courseID FROM Course";
            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                //Retrieve by column name
                int id = rs.getInt("courseID");
                existingCourseID.add(id);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return existingCourseID;
    }

    /**
     * Saves user to the database in table piazzaUser.
     * @param email email address.
     * @param password password.
     */
    public void saveUser(String email, String password){
        try{
            PreparedStatement User = connection.prepareStatement("insert into PiazzaUser(email, password) values (default,(?),(?))");
            User.setString(1, email);
            User.setString(2, password);
            User.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Saves the UserInCourse relation.
     * @param userID UserID.
     * @param courseID CourseID.
     * @param isInstructor isInstructor.
     */
    public void saveUserInCourse(int userID, int courseID, boolean isInstructor){
        try{
            //saves UserInCourse relation
            PreparedStatement UserInCourse = connection.prepareStatement("insert into UserInCourse values ((?),(?),(?))");
            UserInCourse.setInt(1, userID);
            UserInCourse.setInt(2, courseID);
            UserInCourse.setBoolean(3, isInstructor);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Checks if provided email exists in the database.
     * @param email email address.
     * @return true if the email is in the database, false if not.
     */
    public boolean existingEmail(String email){
        email = "'"+email+"'";
        try{
            String queryCheck = " SELECT Email FROM PiazzaUser WHERE Email = " + email;
            PreparedStatement ps = connection.prepareStatement(queryCheck);
            ResultSet rs = ps.executeQuery();
            String useremail ="";
            if (rs.next()){
                useremail = rs.getString(1);
            }
            if (!useremail.isEmpty()){
                return true;
            }
            else {
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }

    /**
     * Checks if the password connected to given email corresponds.
     * @param email email address.
     * @param password password.
     * @return true if password is correct.
     */
    public boolean correctPassword(String email,String password){
        email = "'"+email+"'";
        try{
            String queryCheck = " SELECT UserPassword FROM PiazzaUser WHERE Email = " + email;
            PreparedStatement ps = connection.prepareStatement(queryCheck);
            ResultSet rs = ps.executeQuery();
            String correctPassword ="";
            if (rs.next()){
                correctPassword=rs.getString(1);
            }
            if (correctPassword.equals(password)){
                return true;
            } else {
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }


    /**
     * Finds corresponding UserID from database using email address as input.
     * @param email email address.
     * @return int UserID.
     */
    public int getUserID(String email){
        email = "'"+email+"'";
        try{
            String queryCheck = " SELECT userID FROM PiazzaUser WHERE email = " + email;
            PreparedStatement ps = connection.prepareStatement(queryCheck);
            ResultSet rs = ps.executeQuery();
            int userID = 0;
            if (rs.next()) {
                userID = ((Integer) rs.getObject(1)).intValue();
            }
            return userID;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return 0;
        }
    }


    /**
     * Saves post to database.
     * @param tag Tag for post.
     * @param colorCode colorCode for post.
     * @param folderID FolderID.
     * @param userID UserID.
     */
    public void savePost(String tag, String colorCode, String content, String folderID, int courseID, int userID) {
        try {
            PreparedStatement statement = connection.prepareStatement("insert into Post values (default,?,?,?,?,?,?)");
            statement.setString(1, tag);
            statement.setString(2, colorCode);
            statement.setString(3, content);
            statement.setString(4, folderID);
            statement.setInt(5, courseID);
            statement.setInt(6, userID);
            statement.executeUpdate();
            System.out.println("Post saved.");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("Post unable to save.");
        }
    }


    /**
     * Finds every course from database.
     * @return An Array of all Courses.
     */
    public ArrayList<String> getAllCourses() {
        ArrayList<String> courses = new ArrayList<>();
        try{
            String queryCheck = " SELECT CourseName FROM Course" ;
            PreparedStatement ps = connection.prepareStatement(queryCheck);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String str = rs.getString(1);
                courses.add(str);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return courses;
    }


    /**
     * Finds every folder from database.
     * @return An array of all folders.
     */
    public ArrayList<String> getAllFolders() {
        ArrayList<String> folders = new ArrayList<>();
        try{
            String queryCheck = " SELECT folderName FROM Folder" ;
            PreparedStatement ps = connection.prepareStatement(queryCheck);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String str = rs.getString("folderName");
                folders.add(str);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return folders;
    }

    /**
     * Finds course with given name. Returns ID of course.
     * @param name name.
     * @return int CourseID
     */
    public int getCourseID(String name){
        name = "'"+name+"'";
        try{
            String queryCheck = " SELECT CourseID FROM Course WHERE CourseName = " + name;
            PreparedStatement ps = connection.prepareStatement(queryCheck);
            ResultSet rs = ps.executeQuery();
            int courseID=0;
            if(rs.next()){
                courseID = rs.getInt(1);
            }
            return courseID;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return 0;
        }
    }

    /**
     * Finds course with given folder name. Returns CourseID.
     * @param folderName name of folder.
     * @return int CourseID.
     */
    public int getCourseIDFromFolder(String folderName){
        folderName = "'"+folderName+"'";
        try{
            String queryCheck = " SELECT CourseID FROM Folder WHERE FolderName = " + folderName;
            PreparedStatement ps = connection.prepareStatement(queryCheck);
            ResultSet rs = ps.executeQuery();
            int courseID=0;
            if(rs.next()){
                courseID = rs.getInt(1);
            }
            return courseID;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return 0;
        }
    }

    /**
     * Searches through posts. Finds posts with content containing search word given in input.
     * @param searchWord Search word.
     * @return Array of corresponding posts.
     */
    public ArrayList<Integer> searchPosts(String searchWord){
        searchWord = "'%"+searchWord+"%'";
        ArrayList<Integer> posts = new ArrayList<>();
        try{
            String queryCheck = " SELECT PostID FROM Post WHERE Content LIKE "+searchWord;
            PreparedStatement ps = connection.prepareStatement(queryCheck);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                posts.add(rs.getInt(1));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return posts;
    }

    /**
     * Returns the content of a post with given postID.
     * @param postID postID
     * @return String content.
     */
    public String getPostContent(int postID) {
        String content = "";
        try{
            String queryCheck = " SELECT Content FROM Post Where PostID = "+postID ;
            PreparedStatement ps = connection.prepareStatement(queryCheck);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                content = rs.getString(1);
            }
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return content;
    }

    /**
     * Gets user statistics from database. Puts info into linked hashmap.
     * @return Linked Hashmap of user statistics.
     */
    public LinkedHashMap<String, ArrayList<Integer>> getStats(){
        LinkedHashMap<String, ArrayList<Integer>> userMap = new LinkedHashMap<>();
        try{
            String post = "SELECT UserID, COUNT(*) AS PC FROM Post GROUP BY UserID";
            String viewed = "SELECT UserID, COUNT(*) AS VC FROM Viewed GROUP BY UserID ASC";
            String stats = "(SELECT PU.UserID, IFNULL(V.VC,0), IFNULL(P.PC,0), PU.Email FROM ((PiazzaUser AS PU LEFT JOIN ("+post+") AS P ON PU.UserID=P.UserID) LEFT JOIN (("+viewed+") AS V) ON PU.UserID=V.UserID)) ORDER BY V.VC DESC, P.PC DESC";
            PreparedStatement ps1 = connection.prepareStatement(stats);
            ResultSet rs = ps1.executeQuery();
            while(rs.next()) {
                ArrayList<Integer> userStats = new ArrayList<>();
                userStats.add(rs.getInt(2));
                userStats.add(rs.getInt(3));
                userMap.put(rs.getString(4),userStats);
            }
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return userMap;
    }

    /**
     * Saves reply to database.
     * @param postID postID of the post replied to.
     * @param content content.
     * @param type replyType.
     * @param userID UserID of user posting the reply.
     */
    public void saveReply(int postID, String content, String type, int userID) {
        try {
            PreparedStatement statement = connection.prepareStatement("insert into Reply values (default,?,?,?,?)");
            statement.setInt(1, postID);
            statement.setString(2, content);
            statement.setString(3, type);
            statement.setInt(4, userID);

            statement.executeUpdate();
            System.out.println("Reply saved.");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("Reply unable to save.");
        }
    }

    /**
     * Gets PostID using post content.
     * @param content content of post.
     * @return PostID.
     */
    public int getPostID(String content){
        content = "'" + content + "'";
        try {
            String queryCheck = " SELECT PostID FROM Post WHERE Content = " +content;
            PreparedStatement ps = connection.prepareStatement(queryCheck);
            ResultSet rs = ps.executeQuery();
            int ID = 0;
            if (rs.next()) {
                ID = rs.getInt(1);
            }
            return ID;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return 0;
        }
    }

    /**
     * Checks if the courseID is valid.
     * @param CourseName name of course.
     * @return true if valid, false if not.
     */
    public boolean validCourseID(String CourseName) {
        CourseName="'"+CourseName+"'";
        try {
            String queryCheck = " SELECT courseName FROM Course WHERE courseName = " + CourseName;
            PreparedStatement ps = connection.prepareStatement(queryCheck);
            ResultSet rs = ps.executeQuery();
            boolean result = false;
            if (rs.next()) {
                result = true;
            }
            return result;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }

    /**
     * Checks if User is an instructor.
     * @param userID userID
     * @param courseID courseID
     * @return true if user is instructor, false else.
     */
    public boolean checkIsInstructor(int userID, int courseID) {
        boolean isInstructor = false; //default in database
        try {
            String queryCheck = " SELECT IsInstructor FROM UserInCourse WHERE (UserID = " +userID+ " AND CourseID = " +courseID+")";
            PreparedStatement ps = connection.prepareStatement(queryCheck);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                if(rs.getInt(1)==1) {
                    isInstructor = true;
                }
        }} catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return isInstructor;
    }

    /**
     * Updates the colorCode column in the Post row.
     * @param color ColorCode for post.
     * @param postID PostID.
     */
    public void updateColor(int postID, String color){
        color="'"+color+"'";
        try{
            PreparedStatement Post = connection.prepareStatement("UPDATE Post SET ColorCode = "+color+" WHERE PostID = "+ postID);
            Post.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
