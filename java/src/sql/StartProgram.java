package sql;

import core.Course;
import core.Piazza;
import core.Post;
import core.piazzaUser;
import core.Reply;

import java.sql.SQLException;

/**
 * Class for running the program.
 * Main class for showing the five use cases in action.
 */
public class StartProgram extends DBConn {
    StartProgram() throws SQLException {
        connect();
    }

public static void main(String[] args) throws SQLException {
        StartProgram start = new StartProgram();
        piazzaUser user1 = new piazzaUser("buran@mail.no", "Buran123", "TDT4145", false);
        Course database = new Course("TDT_4145", "spring21");
        Piazza piazza = new Piazza();

        //UseCase 1:
        System.out.print("\n---------------UseCase 1-------------\n");
        piazza.logIn();

        //UseCase 2:
        System.out.print("\n---------------UseCase 2-------------\n");
        System.out.print("Write a post in course "+database.getName()+".\n");
        Post post = new Post(user1, database);
        post.chooseFolder();
        post.setTag();
        post.savePost(post.getTag(), post.getColorCode(), post.readPost(), post.getFolderName(), post.getCourseID(post.getFolderName()), user1.getUserID(user1.getEmail()));

        //UseCase 3:
        System.out.print("\n---------------UseCase 3-------------\n");
        System.out.println("Switched to instructor user");
        piazzaUser user2 = new piazzaUser("instructor@mail.no", "supersecret", "TDT_4145", true);
        String content = post.readPost();
        System.out.println("Write a reply to this post... ");
        System.out.println("'" + content +"'");
        Reply reply = new Reply(user2, post, "reply");
        int postID = post.getPostID(content);
        reply.saveReply(postID, reply.readReply(), reply.getReplyType(), user2.getUserID(user2.getEmail()));


        //UseCase 4:
        System.out.print("\n---------------UseCase 4-------------\n");
        post.searchForPost();

        //UseCase 5:
        System.out.print("\n---------------UseCase 5-------------\n");
        piazza.getStats();

    }
}
